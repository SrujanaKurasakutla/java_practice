package java8features;

import java.util.Optional;

public class OptionalClassExample {
	public static void main(String[] args) {
		String[] str = new String[10];
		str[5] = "SRUJANA";
		Optional<String> checkNull = Optional.ofNullable(str[5]);
		if(checkNull.isPresent()) {
			String lowerCase = str[5].toLowerCase();
			System.out.println(lowerCase);
		}else {
			System.out.println("String is not presented");
		}
		
		checkNull.ifPresent(System.out::println);//using method refernce
		System.out.println(checkNull.get());//using get method
		
		Optional<String> empty = Optional.empty();
		System.out.println(empty);
		
		Optional<String> optional = Optional.of(str[5]);
		
		System.out.println("Filter value : "+optional.filter(value -> value.equals("SRUJANA")).get());
		
		System.out.println("Hash Code : "+optional.hashCode());
		
		System.out.println("Is value present ? "+optional.isPresent());
		
		System.out.println("Using orElse method : "+optional.orElse("else value"));
		
		System.out.println("Using orElse method : "+empty.orElse("else value"));
		
		
	}
}
