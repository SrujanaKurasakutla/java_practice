package java8features;

import pojo.MyInterface2;

public class MethodReference3 {
    public MethodReference3() {
    	System.out.println("Reference to constructor");
    }
	
	public static void main(String[] args) {
		MyInterface2 interface2 = MethodReference3::new;
		interface2.display();

	}

}
