package java8features;

import pojo.MyInterface1;
import pojo.MyInterface2;

public class DefaultNdStaticMethods implements MyInterface1,MyInterface2{

	public static void main(String[] args) {
		DefaultNdStaticMethods object = new DefaultNdStaticMethods();
		object.print();
		MyInterface1.say();
		object.display();
		object.printString("Hello");

	}

	@Override
	public void print() {
		System.out.println("print method");
	}

	@Override
	public void printString(String string) {
		System.out.println("overidden default method : "+string);
	}

	@Override
	public void display() {
		System.out.println("display method");
	}

}
