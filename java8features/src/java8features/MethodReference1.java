package java8features;

import pojo.MyInterface1;

public class MethodReference1 {
    
	public static void printMethod() {
		System.out.println("Reference to static method");
	}
	public static void main(String[] args) {
		MyInterface1 myInterface = MethodReference1::printMethod;
		myInterface.print();
	}

}
