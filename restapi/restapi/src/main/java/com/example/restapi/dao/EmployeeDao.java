package com.example.restapi.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.restapi.model.Employee;



@Repository
public interface EmployeeDao extends CrudRepository<Employee, Long>{
	Optional<Employee> findByFirstName(String employeeName);
}
