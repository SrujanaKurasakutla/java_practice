package com.example.restapi.service;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;

import com.example.restapi.exception.EmployeeNotFoundException;
import com.example.restapi.exception.InvalidEmailException;
import com.example.restapi.model.Employee;

public interface EmployeeService {

	public Iterable<Employee> getAllEmployees();

	public ResponseEntity<Employee> getEmployeeById(Long employeeId) throws EmployeeNotFoundException;

	public Employee createEmployee(@Valid Employee employee) throws InvalidEmailException;

	public ResponseEntity<Employee> updateEmployee(@Valid Employee employee) throws EmployeeNotFoundException;

	public Map<String, Boolean> deleteEmployee(Long employeeId) throws Exception;
	
	
}
