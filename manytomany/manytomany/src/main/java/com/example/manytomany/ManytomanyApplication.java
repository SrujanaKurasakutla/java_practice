package com.example.manytomany;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.manytomany.employee.Employee;
import com.example.manytomany.employee.EmployeeDao;
import com.example.manytomany.project.Project;

@SpringBootApplication
public class ManytomanyApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(ManytomanyApplication.class, args);
	}
    @Autowired
    EmployeeDao employeeDao;
	@Override
	public void run(String... args) throws Exception {
        
        Employee employee1 = new Employee();
        employee1.setFirstName("srujana");
        employee1.setLastName("kurasakutla");

     
        Project project = new Project();
        project.setTitle("Airport Management System");

       
        Project project1 = new Project();
        project1.setTitle("Content Management System");

        
       

        employee1.getProjects().add(project);
        employee1.getProjects().add(project1);
        
       
        project.getEmployees().add(employee1);
        project1.getEmployees().add(employee1);

        employeeDao.save(employee1);
    }
		
	}

