package com.example.onetomany.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.onetomany.model.Department;

public interface DepartmentDao extends JpaRepository<Department, Integer>{

}
