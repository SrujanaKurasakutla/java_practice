package com.example.onetomany.service;

import java.util.List;

import com.example.onetomany.model.Employee;

public interface EmployeeService {

	List<Employee> getAllEmployees();

	Employee addEmployee(Employee employee);

	Employee editEmployees(Employee employee);

	void deleteEmployees(Integer employeeId);

}
