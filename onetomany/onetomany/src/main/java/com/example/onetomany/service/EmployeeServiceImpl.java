package com.example.onetomany.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.onetomany.dao.DepartmentDao;
import com.example.onetomany.dao.EmployeeDao;
import com.example.onetomany.model.Department;
import com.example.onetomany.model.Employee;
@Service
public class EmployeeServiceImpl implements EmployeeService{
    @Autowired
    EmployeeDao employeeDao;
    @Autowired
    DepartmentDao departmentDao;
	@Override
	public List<Employee> getAllEmployees() {
		return employeeDao.findAll();
	}

	@Override
	public Employee addEmployee(Employee employee) {
		Department dept = departmentDao.findById(employee.getDepartment().getId()).orElse(null);
        if (null == dept) {
            dept = new Department();
        }
        dept.setDeptName(employee.getDepartment().getDeptName());
        employee.setDepartment(dept);
        return employeeDao.save(employee);
	}

	@Override
	public Employee editEmployees(Employee employee) {
		return employeeDao.save(employee);
	}

	@Override
	public void deleteEmployees(Integer employeeId) {
		employeeDao.deleteById(employeeId);
		
	}

}
