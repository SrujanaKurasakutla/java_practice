package datetime;

import java.time.*;
import static java.time.Month.*;
import static java.time.temporal.ChronoUnit.*;

public class LocalDateTime1 {

	public static void main(String[] args) {
	LocalDateTime meeting, flight, courseStart, courseEnd;
	meeting = LocalDateTime.of(2021, 8, 18, 11, 55);
	System.out.println("meeting is on "+meeting);
	
	LocalDate flightdate = LocalDate.of(2021, AUGUST, 18);
	LocalTime flightTime = LocalTime.of(11, 55);
	flight = LocalDateTime.of(flightdate, flightTime);
	System.out.println("Flight leaves "+flight);
	
	courseStart = LocalDateTime.of(2021, JULY, 22, 9,0);
	courseEnd = courseStart.plusDays(4).plusHours(8);
	System.out.println("Course starts "+courseStart);
	System.out.println("Course ends "+courseEnd);
	
	long courseHours = (courseEnd.getHour()-courseStart.getHour())*(courseStart.until(courseEnd, DAYS)+1);
	System.out.println("Course is "+courseHours +" hours long");

	}

}
