package datetime;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class Instant1 {
	public static void main(String[] args) {
		Instant instant1 = Instant.now();
		System.out.println(instant1);
		
		Instant instant2 = Instant.parse("2021-07-22T05:13:53.335Z");  
	    System.out.println(instant2); 
	    
	    Instant instant3 = instant2.minus(Duration.ofDays(125));  
	    System.out.println(instant3);  
	    
	    Instant instant4 = instant2.plus(Duration.ofDays(125));  
	    System.out.println(instant4);
	    
	    System.out.println(instant2.isSupported(ChronoUnit.DAYS));  
	    System.out.println(instant2.isSupported(ChronoUnit.YEARS)); 
	}
}
