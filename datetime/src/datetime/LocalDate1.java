package datetime;

import java.time.LocalDate;
import static java.time.temporal.TemporalAdjusters.*;
import static java.time.DayOfWeek.*;


public class LocalDate1 {
public static void main(String[] args) {
	LocalDate now, bDate, nowPlusMonth, nextTues;
	now = LocalDate.now();
	System.out.println("Current Date "+now);
	
	bDate = LocalDate.of(1999, 02, 18);
	System.out.println("Srujana's Bday "+bDate);
	System.out.println("Is Srujana Bday in the Past? "+bDate.isBefore(now));
	System.out.println("Is Srujana's Bday in a Leap Year? "+bDate.isLeapYear());
	System.out.println("Srujana's Bday day of week "+bDate.getDayOfWeek());
	
	nowPlusMonth = now.plusMonths(2);
	System.out.println("The date after two months from now "+nowPlusMonth);
	nextTues = now.with(next(TUESDAY));
	System.out.println("The date of next tuesday "+nextTues);
	
	
}
}
