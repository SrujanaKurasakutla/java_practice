package datetime;

import java.time.LocalTime;
import static java.time.temporal.ChronoUnit.*;


public class LocalTime1 {
public static void main(String[] args) {
	LocalTime now, nowPlus, nowHrsMins, lunch, bedTime;
	now = LocalTime.now();
	System.out.println("The Time now is "+now);
	
	nowPlus = now.plusHours(1).plusMinutes(10);
	System.out.println("What time is it 1hr 10min from now? "+nowPlus);
	
	nowHrsMins = now.truncatedTo(MINUTES);
	System.out.println("Truncate the current time to minutes "+nowHrsMins);
	System.out.println("It is the "+now.toSecondOfDay()/60+ "th minute");
	
	lunch = LocalTime.of(12, 30);
	System.out.println("Is lunch in my future? "+lunch.isAfter(now));
	
	long minsToLunch = now.until(lunch, MINUTES);
	System.out.println("Minutes til lunch "+minsToLunch);
	
	bedTime = LocalTime.of(21, 0);
	long minsToBed = now.until(bedTime, MINUTES);
	System.out.println("Minutes til Bedtime "+minsToBed);
	
}
}
