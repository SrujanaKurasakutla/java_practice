package com.example.onetoone.employee;

import java.util.List;

public interface EmployeeService {

	List<Employee> getAllEmployees();

	Employee addEmployee(Employee employee);

	Employee editEmployees(Employee employee);

	void deleteEmployees(Integer employeeId);

}
