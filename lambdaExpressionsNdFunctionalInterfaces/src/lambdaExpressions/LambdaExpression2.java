package lambdaExpressions;

interface Sum{  
   public int sum(int a,int b);  
}  
public class LambdaExpression2 {
   public static void main(String[] args) {
	Sum sum1 = (int a,int b) -> {
		return a+b;
	};
	System.out.println("Sum is : "+sum1.sum(6, 9));
	
	Sum sum2 = (a,b) -> (a+b);
	System.out.println("Sum is : "+sum2.sum(90,99));
}
}
