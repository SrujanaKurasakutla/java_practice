package lambdaExpressions;

@FunctionalInterface  
interface Shape{  
    public void draw();  
}  

public class LambdaExpression1 {
	public static void main(String[] args) {
		String shape = "square";
		
		Shape shape1 = () -> {
			System.out.println("Shape is : "+shape);
		};
		shape1.draw();
	}
}
