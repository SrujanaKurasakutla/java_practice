package streamexamples;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import product.Product;

public class Stream1 {
	public static void main(String[] args) {
		List<Product> productsList = new ArrayList<Product>();   
        productsList.add(new Product(1,"table",25000f));  
        productsList.add(new Product(2,"bench",30000f));  
        productsList.add(new Product(3,"chair",8000f));  
        productsList.add(new Product(4,"laptop",28000f));  
        productsList.add(new Product(5,"Apple",9f));  
        Stream<Product> stream = productsList.stream();
        List<Float> priceList = stream.filter(product -> product.getPrice() > 3000)
        		                      .map(product -> product.getPrice())
        		                      .collect(Collectors.toList());
        System.out.println(priceList);
        
        
        List<Float> sortedPriceList = productsList.stream().map(product -> product.getPrice())
        		                                  .sorted().collect(Collectors.toList());
        System.out.println(sortedPriceList);
        
        List<Float> reverseSortedPriceList = productsList.stream().map(product -> product.getPrice())
                                                         .sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        System.out.println(reverseSortedPriceList);
        //reduce
        Float totalPrice = productsList.stream().map(product -> product.getPrice())
        		                       .reduce(0f,(sum,price)->sum+price);
        System.out.println("Total price : "+totalPrice);
        
        //count
        System.out.println("Number of Products : "+productsList.stream().count());
        
        //limit
        System.out.println("Top 3 Products");
        productsList.stream().limit(3)
                             .map(product -> product.getName())
                             .forEach(product -> System.out.println(product));
        //skip
        System.out.println("Bottom 3 Products");
        productsList.stream().skip(2)
                             .map(product -> product.getName())
                             .forEach(product -> System.out.println(product));
        
        Float[] priceArray = productsList.stream().map(product -> product.getPrice()).distinct()
        		                                  .toArray(Float[]:: new);
        System.out.println("Price array");
        System.out.println(Arrays.toString(priceArray));
        
        //match
        System.out.println("Is any product name is chair in this product list ? "+ productsList.stream().anyMatch(product -> product.getName().equals("chair")));
        System.out.println("Is all products have common price as 10rs ? "+productsList.stream().allMatch(product -> product.getPrice() == 10));
        System.out.println("Is none of the products have the price of 9rs ? "+productsList.stream().noneMatch(product -> product.getPrice() == 9));
        
        
        Optional<Product> first = productsList.stream().filter(product -> product.getName().startsWith("c")).findFirst();
        System.out.println("The first product in this product list : "+first.get().getName());
        System.out.println("Give the Random product name : "+productsList.stream().findAny().get().getName());
        
        Product minProduct = productsList.stream().min((product1,product2) -> product1.getPrice() > product2.getPrice() ? 1 : -1).get();
        System.out.println("Lowest product wrt to price : "+minProduct.getName()+" - "+minProduct.getPrice());
        
        Product maxProduct = productsList.stream().max((product1,product2) -> product1.getPrice() < product2.getPrice() ? -1 : 1).get();
        System.out.println("Highest product wrt to price : "+maxProduct.getName()+" - "+maxProduct.getPrice());
       
        double sum = productsList.stream().mapToDouble(product -> product.getPrice()).sum();
        System.out.println("Sum of prices of the products : "+sum);
        
        Set<Float> productPriceList =   
                productsList.stream()  
                            .map(Product::getPrice)           
                            .collect(Collectors.toSet());   
        System.out.println(productPriceList);  
        
        
	}
}
